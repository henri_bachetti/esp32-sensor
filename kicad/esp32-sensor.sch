EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR03
U 1 1 61A7826A
P 3850 2900
F 0 "#PWR03" H 3850 2750 50  0001 C CNN
F 1 "+3.3V" H 3850 3040 50  0000 C CNN
F 2 "" H 3850 2900 50  0001 C CNN
F 3 "" H 3850 2900 50  0001 C CNN
	1    3850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2900 3850 3000
Wire Wire Line
	3850 3000 3950 3000
$Comp
L power:GND #PWR05
U 1 1 61A782DA
P 5900 4350
F 0 "#PWR05" H 5900 4100 50  0001 C CNN
F 1 "GND" H 5900 4200 50  0000 C CNN
F 2 "" H 5900 4350 50  0001 C CNN
F 3 "" H 5900 4350 50  0001 C CNN
	1    5900 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4150 5900 4250
Wire Wire Line
	5900 4250 5900 4350
Wire Wire Line
	5900 4150 5800 4150
Wire Wire Line
	5800 4250 5900 4250
Connection ~ 5900 4250
$Comp
L power:GND #PWR06
U 1 1 61A7833C
P 4450 4750
F 0 "#PWR06" H 4450 4500 50  0001 C CNN
F 1 "GND" H 4450 4600 50  0000 C CNN
F 2 "" H 4450 4750 50  0001 C CNN
F 3 "" H 4450 4750 50  0001 C CNN
	1    4450 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4650 4450 4750
$Comp
L esp32-sensor-rescue:Battery_Cell BT1
U 1 1 61A7838E
P 5650 5850
F 0 "BT1" H 5450 6000 50  0000 L CNN
F 1 "LIPO-102535" H 5450 5800 50  0000 L CNN
F 2 "myBatteries:LIPO-102535" V 5650 5910 50  0001 C CNN
F 3 "" V 5650 5910 50  0001 C CNN
	1    5650 5850
	1    0    0    -1  
$EndComp
$Comp
L my-batteries:TP4056-MODULE U2
U 1 1 61A7859E
P 4600 5650
F 0 "U2" H 4500 5550 60  0000 C CNN
F 1 "TP4056-MODULE" H 4450 5650 60  0000 C CNN
F 2 "myModules:TP4056-CHARGER" H 4600 5650 60  0001 C CNN
F 3 "" H 4600 5650 60  0000 C CNN
	1    4600 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5550 5650 5550
Wire Wire Line
	5650 5300 5650 5550
Wire Wire Line
	5650 5550 5650 5650
Wire Wire Line
	5150 5750 5350 5750
Wire Wire Line
	5350 5750 5350 6050
Wire Wire Line
	5350 6050 5650 6050
Wire Wire Line
	5650 6050 5650 5950
$Comp
L power:GND #PWR07
U 1 1 61A78653
P 5250 6000
F 0 "#PWR07" H 5250 5750 50  0001 C CNN
F 1 "GND" H 5250 5850 50  0000 C CNN
F 2 "" H 5250 6000 50  0001 C CNN
F 3 "" H 5250 6000 50  0001 C CNN
	1    5250 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5900 5250 5900
Wire Wire Line
	5250 5900 5250 6000
$Comp
L my-regulators:ME6211C U3
U 1 1 61A789CF
P 6750 5500
F 0 "U3" H 6450 5750 50  0000 L CNN
F 1 "RT9013" H 6600 5750 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 6750 5825 50  0001 C CNN
F 3 "" H 6750 5600 50  0001 C CNN
	1    6750 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5400 6450 5400
Wire Wire Line
	6350 5400 6350 5500
Wire Wire Line
	6350 5500 6350 5600
Wire Wire Line
	6350 5500 6450 5500
$Comp
L power:+3.3V #PWR08
U 1 1 61A78B53
P 7150 5300
F 0 "#PWR08" H 7150 5150 50  0001 C CNN
F 1 "+3.3V" H 7150 5440 50  0000 C CNN
F 2 "" H 7150 5300 50  0001 C CNN
F 3 "" H 7150 5300 50  0001 C CNN
	1    7150 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 5300 7150 5400
Wire Wire Line
	7150 5400 7150 5600
Wire Wire Line
	7150 5400 7050 5400
$Comp
L power:GND #PWR09
U 1 1 61A78BEB
P 6750 6100
F 0 "#PWR09" H 6750 5850 50  0001 C CNN
F 1 "GND" H 6750 5950 50  0000 C CNN
F 2 "" H 6750 6100 50  0001 C CNN
F 3 "" H 6750 6100 50  0001 C CNN
	1    6750 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 5800 6750 6000
Wire Wire Line
	6750 6000 6750 6100
$Comp
L my-power:+VBATT #PWR010
U 1 1 61A7A436
P 5650 5300
F 0 "#PWR010" H 5650 5150 50  0001 C CNN
F 1 "+VBATT" H 5650 5450 50  0000 C CNN
F 2 "" H 5650 5300 50  0000 C CNN
F 3 "" H 5650 5300 50  0000 C CNN
	1    5650 5300
	1    0    0    -1  
$EndComp
Connection ~ 5650 5550
$Comp
L my-power:+VBATT #PWR011
U 1 1 61A7A4BB
P 2950 2150
F 0 "#PWR011" H 2950 2000 50  0001 C CNN
F 1 "+VBATT" H 2950 2300 50  0000 C CNN
F 2 "" H 2950 2150 50  0000 C CNN
F 3 "" H 2950 2150 50  0000 C CNN
	1    2950 2150
	1    0    0    -1  
$EndComp
$Comp
L esp32-sensor-rescue:R R2
U 1 1 61A7A500
P 2950 2900
F 0 "R2" V 3030 2900 50  0000 C CNN
F 1 "1M" V 2950 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2880 2900 50  0001 C CNN
F 3 "" H 2950 2900 50  0001 C CNN
	1    2950 2900
	1    0    0    -1  
$EndComp
$Comp
L esp32-sensor-rescue:R R1
U 1 1 61A7A551
P 2950 2400
F 0 "R1" V 3030 2400 50  0000 C CNN
F 1 "1M" V 2950 2400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2880 2400 50  0001 C CNN
F 3 "" H 2950 2400 50  0001 C CNN
	1    2950 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2150 2950 2250
$Comp
L power:GND #PWR012
U 1 1 61A7A609
P 2950 3150
F 0 "#PWR012" H 2950 2900 50  0001 C CNN
F 1 "GND" H 2950 3000 50  0000 C CNN
F 2 "" H 2950 3150 50  0001 C CNN
F 3 "" H 2950 3150 50  0001 C CNN
	1    2950 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3050 2950 3150
$Comp
L esp32-sensor-rescue:C C1
U 1 1 61A8CE6F
P 6350 5750
F 0 "C1" H 6375 5850 50  0000 L CNN
F 1 "1µF MLCC" H 6200 5650 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210_HandSoldering" H 6388 5600 50  0001 C CNN
F 3 "" H 6350 5750 50  0001 C CNN
	1    6350 5750
	1    0    0    -1  
$EndComp
Connection ~ 6350 5500
Connection ~ 7150 5400
Wire Wire Line
	6350 5900 6350 6000
Wire Wire Line
	6350 6000 6750 6000
Wire Wire Line
	6750 6000 7150 6000
Connection ~ 6750 6000
Wire Wire Line
	7150 6000 7150 5900
Wire Wire Line
	3900 3150 3900 3400
Wire Wire Line
	3900 3400 3950 3400
Wire Wire Line
	5800 4050 6000 4050
Wire Wire Line
	6250 2200 5900 2200
Wire Wire Line
	5900 2200 5900 3050
Wire Wire Line
	5900 3050 5800 3050
Wire Wire Line
	6150 2250 5950 2250
Wire Wire Line
	5950 2250 5950 3150
Wire Wire Line
	5950 3150 5800 3150
Wire Wire Line
	6450 1450 6450 1850
Wire Wire Line
	6450 2300 6000 2300
Wire Wire Line
	6000 2300 6000 4050
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 6219378B
P 5950 5400
F 0 "JP1" H 5800 5350 50  0000 C CNN
F 1 "AMP" H 6000 5350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5950 5400 50  0001 C CNN
F 3 "~" H 5950 5400 50  0001 C CNN
	1    5950 5400
	1    0    0    1   
$EndComp
Wire Wire Line
	6050 5400 6150 5400
Connection ~ 6350 5400
Connection ~ 6150 5400
Wire Wire Line
	6150 5400 6350 5400
Wire Wire Line
	6250 2200 6250 1650
Wire Wire Line
	6350 2150 6350 1750
Wire Wire Line
	6150 2250 6150 1550
Wire Wire Line
	3650 2150 3650 3100
Wire Wire Line
	3650 2150 6350 2150
Wire Wire Line
	3650 3100 3950 3100
$Comp
L power:+3.3V #PWR01
U 1 1 61A78135
P 6850 2850
F 0 "#PWR01" H 6850 2700 50  0001 C CNN
F 1 "+3.3V" H 6850 2990 50  0000 C CNN
F 2 "" H 6850 2850 50  0001 C CNN
F 3 "" H 6850 2850 50  0001 C CNN
	1    6850 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2950 6850 2950
Wire Wire Line
	6400 2950 5800 2950
Wire Wire Line
	6950 3150 6400 3150
Wire Wire Line
	6400 3150 6400 2950
$Comp
L power:GND #PWR02
U 1 1 61A7817B
P 6850 4650
F 0 "#PWR02" H 6850 4400 50  0001 C CNN
F 1 "GND" H 6850 4500 50  0000 C CNN
F 2 "" H 6850 4650 50  0001 C CNN
F 3 "" H 6850 4650 50  0001 C CNN
	1    6850 4650
	1    0    0    -1  
$EndComp
Text GLabel 6400 2550 1    50   Input ~ 0
SCL
Wire Wire Line
	6400 2950 6400 2550
Connection ~ 6400 2950
Text GLabel 6100 4750 3    50   Input ~ 0
MISO
Text GLabel 6200 4750 3    50   Input ~ 0
MOSI
Wire Wire Line
	6300 3250 6950 3250
Wire Wire Line
	5800 3250 6300 3250
Connection ~ 6300 3250
Text GLabel 6300 2550 1    50   Input ~ 0
SDA
Wire Wire Line
	6300 2550 6300 3250
Wire Wire Line
	6200 2850 5800 2850
Text GLabel 6300 4750 3    50   Input ~ 0
SCK
Wire Wire Line
	5800 3550 6300 3550
Text GLabel 6400 4750 3    50   Input ~ 0
SS
Wire Wire Line
	6100 3450 5800 3450
Wire Wire Line
	6600 3750 5800 3750
Wire Wire Line
	6600 3450 6600 3750
Wire Wire Line
	6400 4750 6400 4500
Wire Wire Line
	6400 3650 5800 3650
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 6227F2D1
P 7150 4200
F 0 "J4" H 7230 4192 50  0000 L CNN
F 1 "SPI" H 7230 4101 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 7150 4200 50  0001 C CNN
F 3 "~" H 7150 4200 50  0001 C CNN
	1    7150 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4000 6800 4000
Wire Wire Line
	6800 4000 6800 2950
Wire Wire Line
	6950 4500 6400 4500
Connection ~ 6400 4500
Wire Wire Line
	6400 4500 6400 3650
Wire Wire Line
	6950 3350 6500 3350
Wire Wire Line
	6300 3550 6300 4400
Wire Wire Line
	6200 2850 6200 4200
Wire Wire Line
	6100 3450 6100 4300
Wire Wire Line
	6950 4400 6300 4400
Connection ~ 6300 4400
Wire Wire Line
	6300 4400 6300 4750
Wire Wire Line
	6950 4300 6100 4300
Connection ~ 6100 4300
Wire Wire Line
	6100 4300 6100 4750
Wire Wire Line
	6950 4200 6200 4200
Connection ~ 6200 4200
Wire Wire Line
	6200 4200 6200 4750
Text GLabel 6500 2550 1    50   Input ~ 0
GPIO16
Wire Wire Line
	6500 2550 6500 3350
Wire Wire Line
	6850 4100 6850 4650
Wire Wire Line
	6950 4100 6850 4100
Connection ~ 6850 4100
Wire Wire Line
	6850 3050 6950 3050
Wire Wire Line
	6850 3050 6850 4100
Text GLabel 6600 2550 1    50   Input ~ 0
GPIO17
Wire Wire Line
	6600 2550 6600 3450
Connection ~ 6600 3450
Wire Wire Line
	6600 3450 6950 3450
Wire Wire Line
	5800 3850 6500 3850
Wire Wire Line
	6500 3850 6500 3350
Connection ~ 6500 3350
Text GLabel 5800 1550 0    50   Input ~ 0
RX
Text GLabel 5800 1650 0    50   Input ~ 0
TX
Text GLabel 5800 1750 0    50   Input ~ 0
EN
Text GLabel 5800 1850 0    50   Input ~ 0
IO0
Connection ~ 6450 1850
Wire Wire Line
	6450 1850 6450 2300
Wire Wire Line
	5800 1850 6450 1850
Wire Wire Line
	6050 1950 6050 1450
$Comp
L power:GND #PWR018
U 1 1 61A9D5F8
P 6050 1950
F 0 "#PWR018" H 6050 1700 50  0001 C CNN
F 1 "GND" H 6050 1800 50  0000 C CNN
F 2 "" H 6050 1950 50  0001 C CNN
F 3 "" H 6050 1950 50  0001 C CNN
	1    6050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1750 6350 1750
Connection ~ 6350 1750
Wire Wire Line
	6350 1750 6350 1450
Wire Wire Line
	6250 1650 5800 1650
Connection ~ 6250 1650
Wire Wire Line
	6250 1650 6250 1450
Wire Wire Line
	5800 1550 6150 1550
Connection ~ 6150 1550
Wire Wire Line
	6150 1550 6150 1450
$Comp
L Connector_Generic:Conn_01x07 J2
U 1 1 62328CE6
P 7150 3250
F 0 "J2" H 7230 3292 50  0000 L CNN
F 1 "I2C" H 7230 3201 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 7150 3250 50  0001 C CNN
F 3 "~" H 7150 3250 50  0001 C CNN
	1    7150 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3550 6700 3550
Wire Wire Line
	6700 3550 6700 3950
Wire Wire Line
	6700 3950 5800 3950
Text GLabel 6700 2550 1    50   Input ~ 0
GPIO4
Wire Wire Line
	6700 2550 6700 3550
Connection ~ 6700 3550
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 6235D638
P 2900 3900
F 0 "J1" H 3000 3750 50  0000 C CNN
F 1 "GPIO" H 3050 3650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 2900 3900 50  0001 C CNN
F 3 "~" H 2900 3900 50  0001 C CNN
	1    2900 3900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3950 4300 3900 4300
Wire Wire Line
	3500 3600 3500 3500
$Comp
L power:+3.3V #PWR013
U 1 1 61A8F7A7
P 3500 3500
F 0 "#PWR013" H 3500 3350 50  0001 C CNN
F 1 "+3.3V" H 3500 3640 50  0000 C CNN
F 2 "" H 3500 3500 50  0001 C CNN
F 3 "" H 3500 3500 50  0001 C CNN
	1    3500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3600 3500 3600
Wire Wire Line
	3550 3800 3550 3600
Wire Wire Line
	3100 3800 3300 3800
Wire Wire Line
	3600 3700 3600 3900
Wire Wire Line
	3600 3900 3400 3900
Wire Wire Line
	3700 3900 3700 4100
Wire Wire Line
	3950 3900 3700 3900
Wire Wire Line
	3700 4100 3600 4100
Wire Wire Line
	3750 4200 3700 4200
$Comp
L power:GND #PWR04
U 1 1 61A782A1
P 3050 4500
F 0 "#PWR04" H 3050 4250 50  0001 C CNN
F 1 "GND" H 3050 4350 50  0000 C CNN
F 2 "" H 3050 4500 50  0001 C CNN
F 3 "" H 3050 4500 50  0001 C CNN
	1    3050 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3800 3650 3800
Wire Wire Line
	3950 3700 3600 3700
Wire Wire Line
	3550 3600 3950 3600
Wire Wire Line
	3750 4000 3950 4000
Wire Wire Line
	3750 4000 3750 4200
$Comp
L my-microcontrollers:ESP32-WROOM U1
U 1 1 61A77DF6
P 4900 3600
F 0 "U1" H 4450 4850 60  0000 C CNN
F 1 "ESP32-WROOM" H 5100 4850 60  0000 C CNN
F 2 "myMicroControllers:ESP32-WROOM-32" H 5250 4950 60  0001 C CNN
F 3 "" H 4450 4050 60  0001 C CNN
	1    4900 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4100 3800 4100
Wire Wire Line
	3800 4100 3800 4300
Text GLabel 3300 4750 3    50   Input ~ 0
IO32
Wire Wire Line
	2950 2550 2950 2650
Wire Wire Line
	2950 2650 3250 2650
Wire Wire Line
	3600 2650 3600 3150
Connection ~ 2950 2650
Wire Wire Line
	3600 3150 3900 3150
Wire Wire Line
	2950 2650 2950 2750
Wire Wire Line
	3300 4750 3300 3800
Connection ~ 3300 3800
Wire Wire Line
	3300 3800 3550 3800
Text GLabel 3400 4750 3    50   Input ~ 0
IO33
Text GLabel 3500 4750 3    50   Input ~ 0
IO25
Text GLabel 3600 4750 3    50   Input ~ 0
IO26
Text GLabel 3700 4750 3    50   Input ~ 0
IO27
Text GLabel 3800 4750 3    50   Input ~ 0
IO14
Wire Wire Line
	3050 4400 3050 4500
Wire Wire Line
	3900 4400 3900 4300
Connection ~ 3400 3900
Wire Wire Line
	3400 3900 3100 3900
Wire Wire Line
	3650 3800 3650 4000
Wire Wire Line
	3650 4000 3500 4000
Wire Wire Line
	3400 3900 3400 4750
Wire Wire Line
	3200 3700 3200 4400
Wire Wire Line
	3200 3700 3100 3700
Connection ~ 3200 4400
Wire Wire Line
	3200 4400 3050 4400
Wire Wire Line
	3200 4400 3900 4400
Connection ~ 3500 4000
Wire Wire Line
	3500 4000 3100 4000
Wire Wire Line
	3500 4000 3500 4750
Connection ~ 3600 4100
Wire Wire Line
	3600 4100 3100 4100
Wire Wire Line
	3600 4100 3600 4750
Connection ~ 3700 4200
Wire Wire Line
	3700 4200 3100 4200
Wire Wire Line
	3700 4200 3700 4750
Wire Wire Line
	3100 4300 3800 4300
Connection ~ 3800 4300
Wire Wire Line
	3800 4300 3800 4750
Wire Wire Line
	5150 5400 5750 5400
Connection ~ 5750 5400
Wire Wire Line
	5750 5400 5850 5400
Wire Wire Line
	6150 5650 6150 5400
Wire Wire Line
	5750 5550 5750 5400
$Comp
L Switch:SW_SPDT SW1
U 1 1 62594646
P 5950 5650
F 0 "SW1" H 6000 5650 50  0000 C CNN
F 1 "PWR" H 5900 5750 50  0000 C CNN
F 2 "mySwitches:DIP-SWITCH" H 5950 5650 50  0001 C CNN
F 3 "~" H 5950 5650 50  0001 C CNN
	1    5950 5650
	-1   0    0    -1  
$EndComp
$Comp
L esp32-sensor-rescue:C C3
U 1 1 621BF40E
P 3250 2900
F 0 "C3" H 3275 3000 50  0000 L CNN
F 1 "10nF" H 3100 2800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3288 2750 50  0001 C CNN
F 3 "" H 3250 2900 50  0001 C CNN
	1    3250 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2750 3250 2650
Connection ~ 3250 2650
Wire Wire Line
	3250 2650 3600 2650
$Comp
L power:GND #PWR0101
U 1 1 621C6886
P 3250 3150
F 0 "#PWR0101" H 3250 2900 50  0001 C CNN
F 1 "GND" H 3250 3000 50  0000 C CNN
F 2 "" H 3250 3150 50  0001 C CNN
F 3 "" H 3250 3150 50  0001 C CNN
	1    3250 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3050 3250 3150
$Comp
L Device:CP C2
U 1 1 62216651
P 7150 5750
F 0 "C2" H 7268 5796 50  0000 L CNN
F 1 "22µF" H 7268 5705 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 7188 5600 50  0001 C CNN
F 3 "~" H 7150 5750 50  0001 C CNN
	1    7150 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2950 6850 2850
Connection ~ 6850 2950
Wire Wire Line
	6850 2950 6800 2950
$Comp
L esp32-sensor-rescue:Conn_01x05 J3
U 1 1 61A9D512
P 6250 1250
F 0 "J3" H 6250 1550 50  0000 C CNN
F 1 "FTDI" V 6250 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x05_Pitch2.54mm" H 6250 1250 50  0001 C CNN
F 3 "" H 6250 1250 50  0001 C CNN
	1    6250 1250
	0    1    -1   0   
$EndComp
$EndSCHEMATC
